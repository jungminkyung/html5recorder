var _html5recorder : Html5Recorder;
class Html5Recorder {
    public audioDefaultConstraintString : Object;
    public videoDefaultConstraintString : Object;
    public constraints : Object;
    public cameraErrorHandler : Function;   //카메라 활성화 오류시 실행할 함수
    public cameraSuccessHandler : Function; //카메라 활성화 성공시 실행할 함수.
    public self : Html5Recorder;    
    public myvideoid : string; //내 화면 video를 만들 타겟 id(parentdiv)
    public playervideoid : string; //영상플레이용 video를 만들 타겟 id(parentdiv)
    public selectedvideoid : string; //옵션에서 카메라 선택시, 선택된 카메라 시그니쳐.
    public selectedaudioid : string; //옵션에서 오디오 선택시, 선택된 오디오 시그니쳐.
    public width : number = 640;   //내화면 가로
    public height : number = 480;  //내화면세로
    public playvideowidth : number = 640; //플레이용비디오 가로
    public playvideoheight : number = 480; //플레이용비디오 세로
    public myvideo : any = '';       //내 화면 재생용 video태그가 생성되었을때 그 DOM노드
    public cameracan : Boolean = false; //카메라 사용가능. 카메라가 로드되면 true
    public soundcan : Boolean = false; //사운드사용가능. 사운드가 로드되면 true
    public getRequestExecuted : Boolean = false; // getRequest()를 실행해서 넘어온인자값들을 저장했는지여부.    
    public phaseChain : Array<string> = new Array();    //페이즈가 저장되어있는 배열. 꺼내어 쓴다.
    public phaseChainSub : Array<string> = new Array();  //페이즈진행중 하위페이즈배열. info를 읽어서 질문개수만큼 push 한다. 꺼내어 쓴다.
    public currentphase : string ;  //현재 페이즈.
    public currentsubphase : string; //현재 서브페이즈
    public b_getsetinfoed : Boolean = false;  //getsetinfo를 실행했는지 여부. 한번만 실행함.
    public textanswer2_heightcheck_timer : number = null

    public qu_list : Array<number>; 
    public qu_list_count : number;
    public qu_list_clip : String;
    public qu_list_clip_alpha : String;  
    public currentpositionQuestion : number =0; //현재위치 처음은0 첫번째 질문을 선택할때도 0(배열)

    public an_list : Array<number>;
    public an_list_clip : String;
    public questiondata : Object; //질문데이터, getsetinfo를 할때 필수이다.
    
    //배열저장값들
    public json_data : JSONDATA;  //getsetinfo의 결과 
    public request : RequestData; //extract값들.    

    //마우스
    public mousehandler_added : boolean = false;
    public textansweralert_dragabled : boolean = false;
    public mousex : number=0;
    public mousey : number=0;
    public mouseclickable : boolean = false;
    public mousemove_prey : number = 0;
    public dragging : boolean = false;  //마우스가 누른채로 이동중인지
    public mouse_starttop : number; //마우스를 눌렀을때 타겟의 현재 Top. (뗄때까지 바뀌지 않음)
    public mouse_starty : number;   //마우스를 눌렀을때 시작점
    public mouse_startheight : number; //마우스를 눌렀을때 타겟의 현재 height
    public textansweralertismoving : boolean; //textansweralert이 이동중인지. 마우스를 떼면 false가 됨.
    public textansweralertissizeup : boolean; //textansweralert이 위쪽으로 사이즈업중인지. 마우스를 떼면 false가 됨
    
    public nocammode : boolean = false;
    //녹화
    public recordedBlobs : any;
    public mediaRecorder : any;
    public datacount : number;
    public isrecording : boolean = false;
    public sound100array : Array<String|Blob> = new Array();
    public remaintime : number; //녹화 남은시간. 60초 녹화일때 60부터 1씩 줄어든다.
    /*
    * 생성시 사운드는 꺼진상태가 Default 켜기위해서는 soundon()
    */
    constructor(myvideoid:string , playervideoid:string , cameraSuccessHandler2 = function() {} , cameraErrorHandler2=function(){}) {
        _html5recorder = this;
        this.self = this;
        this.cameraErrorHandler = cameraErrorHandler2; //카메라 에러 핸들러 셋팅
        this.cameraSuccessHandler = cameraSuccessHandler2; //카메라 성공 핸들러 셋팅
        this.myvideoid = myvideoid;
        this.playervideoid = playervideoid;

        this.audioDefaultConstraintString = {  
            "sampleSize": 16,
            "channelCount": 2,
            "echoCancellation": true 
            };
        this.videoDefaultConstraintString = {
            "width": 0,
            "height": 0,
            "frameRate": 30 
            };
        this.constraints = {
            audio: this.audioDefaultConstraintString,          
            video: this.videoDefaultConstraintString          
        };    
        navigator.mediaDevices.getUserMedia(this.constraints).then(this.getUserMediaSuccess).catch(this.getUserMediaError);
        this.add_mousehandler();
        /*window.addEventListener('error',function(e){
            var ie:any = window.event || {};
            var errMsg = e.message || ie.errorMessage || "404 error on " + window.location;
            var errSrc = (e.filename || ie.errorUrl) + ': ' + (e.lineno || ie.errorLine);
            console.log(errMsg,'errMsg');
            console.log(errSrc,'errSrc');
        });*/
    } 
    /*
   * 질문재생 서브페이즈 TODO. 실행후 서브페이즈가 남아있을시 shift. 없을시 서브페이즈를 종료하고 메인페이즈를 실행한다.
   * questionintro,questionPlay,textanswerview,movingmp4,toronqtime,answer,answersave,questionoutro
   */
   public sub_questionPlaySubPhaseShift() {
        console.log('questionPlaySubPhase');
        var subphase = this.phaseChainSub.shift();
        this.currentsubphase = subphase;
        console.log("서브페이즈 : "+ subphase);
        switch(subphase) {
            case 'questionintro':
                //questionintro가 있을때 재생한다.
                this.sub_questionPlaySubPhaseShift();
              break;
            case 'questionplay':
                this.sub_questionPlaySubPhase();
              break;
            case 'textanswerview' :
                this.sub_textanswerviewSubPhase();
              break;  
            case 'movingmp4':
                this.sub_movingmp4SubPhase();
              break;
            case 'toronqtime':
                this.sub_toronqtimeSubPhase();
              break;
            case 'answerrecord':
                this.sub_answerRecordSubPhase();
              break;
            case 'answersave':
                this.sub_answerSaveSubPhase();
              break;
            case 'questionoutro':
                this.sub_questionPlaySubPhaseShift();
              break; 
            case 'nextquestion':
                //이 페이즈가 없으면 질문이 다음으로 넘어가지 않는다.
                this.currentpositionQuestion++;
                this.log("다음 질문으로 이동",'green');
                console.log("현재질문위치 : "+  this.currentpositionQuestion);
                console.log("현재질문위치 re: "+  _html5recorder.currentpositionQuestion);
                this.sub_questionPlaySubPhaseShift();
                break;
            case undefined:
            case 'undefined':
            case 'null':
              console.log("서브페이즈 끝. 다음 메인페이즈로 이동");
              this.nextPhase();
              break;
            default:
              _html5recorder.log(subphase + '지정되있지 않음. 오류. 다음으로 넘어간다. ','red');
              this.sub_questionPlaySubPhaseShift();
        }       
    }
    /*
    * 다음으로이동. 남은 녹화시간중에 누르면 남은 녹화시간이 끝나고 다음으로 이동함.
    */
    public nextQuestion() {

        this.remaintime = 0;
    }
    /*
    * 대기시간 서브페이즈
    */
    public sub_toronqtimeSubPhase() {
        this.toronqtime().then ( ()=>{
            this.sub_questionPlaySubPhaseShift();
        });
    }
    public sub_answerSaveSubPhase() {
        this.answerSave().then( ()=>{
          console.log("저장완료. 다음서브페이즈로 진행");
           this.sub_questionPlaySubPhaseShift();
        } );
    }
    /*
    * 답변녹화 서브 페이즈. 준비후 녹화 진행
    */
    public sub_answerRecordSubPhase() {
        console.log('sub_answerRecordSubPhase','sub_answerRecordSubPhase');        
        this.answerRecord().then( ()=>{
          this.answerRecordStop();          
        }).then( ()=>{
          this.sub_questionPlaySubPhaseShift();
        });
    }
    /*
    * wait_time이 줄어들면서 답변을 녹화한다.
    */
    public answerRecord() {
        var $deferred = $.Deferred();        
        let wait_time = this.json_data['qulist'][ this.currentpositionQuestion ]['wait_time'];
        let recordedtime = 0;
        let minrectime = this.json_data['qulist'][ this.currentpositionQuestion ]['minrectime'];
        if(!minrectime) {
            this.log("minrectime이 없습니다. 오류. position:"+ this.currentpositionQuestion,'red');
            alert('최소 녹화시간이 없습니다. 오류입니다.');
            $deferred.resolve();
            return $deferred.promise();
        }
        if(!wait_time) {
            this.log("wait_time이 없습니다. 오류. position:"+ this.currentpositionQuestion,'red');
            alert('최대 녹화시간이 없습니다. 오류입니다.');
            $deferred.resolve();
            return $deferred.promise();
        }
        this.wait_time_draw(wait_time);
        this.remaintime = wait_time;
        let remaintime_decrese_timer = setInterval( ()=>{
            this.remaintime--;
            recordedtime++;
            this.wait_time_draw(this.remaintime);
            if(this.remaintime <= 0) {                
                clearInterval(remaintime_decrese_timer);
                remaintime_decrese_timer = null;  
                console.log("resolve"); 
                return $deferred.resolve();             
            }
            console.log(minrectime +':'+ recordedtime);
            if(minrectime == recordedtime) {
                //스킵버튼                
                $('#skipbutton').css('display','');
            }

        }, 1000);

        //녹화시작 전 검사
        if( this.myvideo == '' ) {
            this.log("녹화진행 answerRecord 페이즈에서 카메라가 활성화되있지 않은것 같다.",'red');            
            console.log("resolve");
            $deferred.resolve();            
            return $deferred.promise();
        }

        this.myvideo.volumn = 1;
        this.recordedBlobs = []; 
        this.datacount = 0;  
        var options = {mimeType: 'video/mp4;codecs=vp9'};
        if ( !MediaRecorder.isTypeSupported(options.mimeType)) {
          console.log(options.mimeType + ' is not Supported');
          options = { mimeType: 'video/mp4;codecs=vp8' };
          if (!MediaRecorder.isTypeSupported(options.mimeType)) {
            console.log(options.mimeType + ' is not Supported');
            options = { mimeType: 'video/mp4' };
            if (!MediaRecorder.isTypeSupported(options.mimeType)) {
              console.log(options.mimeType + ' is not Supported');
              options = {mimeType: ''};
            }
          }
        }
        try {        
            this.mediaRecorder = new MediaRecorder(window.stream, options);      
            console.log(this.mediaRecorder);                
        } catch (e) {              
            if(_html5recorder.nocammode == true){
            }
            else{
                console.error('Exception while creating MediaRecorder: ' + e);
                alert('Exception while creating MediaRecorder: '   + e + '. mimeType: ' + options.mimeType);
                return;
            }        
        }
          
        this.mediaRecorder.onstart = function(){ _html5recorder.log(_html5recorder.mediaRecorder.state,'green') }; //recording
        //this.mediaRecorder.onstop = function(){ console.log(_html5recorder.mediaRecorder.state) }; //inactive
        //this.mediaRecorder.onpause = function(){ console.log(_html5recorder.mediaRecorder.state) }; //paused
        //this.mediaRecorder.onresume = function(){ console.log(_html5recorder.mediaRecorder.state) }; //recording
        
        //ondataavailable에서 사용할 타이머를 미리 초기화
        /*if(_html5recorder.wait_timeTimerInstance != null){
          clearInterval(_html5recorder.wait_timeTimerInstance);
        } */         
        //이 리스너는 다중설치가 가능함. 한번만 설치하게 수정          
        delete this.mediaRecorder.ondataavailable;
        this.mediaRecorder.ondataavailable = _html5recorder.answerRecordData;            
        this.mediaRecorder.start(10); // collect 10ms of data
        _html5recorder.isrecording = true;          
        console.log('MediaRecorder started', this.mediaRecorder);            
        //녹화중지시 바깥에서 _html5recorder.closeCamera();   _html5recorder.isrecording = false; 를 호출한다.            
        $('#recordstatediv').text('녹화중');              
        //_html5recorder.video.volume = 0;  //Video를 생성했다고 해서 볼륨이 0에서 자동으로 1로 되돌아오지않음                                  
        delete _html5recorder.myvideo.onended;
        return $deferred.promise();
    }
    /*
    * 녹화를 중지한다. 카메라를 켜거나 끄지 않음. mediaRecorder를 삭제한다.
    */
    public answerRecordStop() {
        console.log("recordstop");
        
        this.isrecording = false;    
        try{
            _html5recorder.mediaRecorder.stop();
            delete this.mediaRecorder;    //한번만든뒤 시작중지만 하는것이 아니라, 만든뒤에 중지할때 삭제해준다. 문제가 생길시 삭제하지 않게 변경하자.
        }catch(e){
            console.log(e);
        }
        _html5recorder.myvideo.volume = 0;    
    }
    /*
    * 미디어레코더가 생성준비되었을때 이벤트 
    */
    public answerRecordData(event) {
        _html5recorder.datacount++;  
         if (event.data && event.data.size > 0) {
          var rand = Math.floor((Math.random() * 1000) + 1);          
          //레코딩플래그가 1일때만 카메라의 enable이 false일때도 데이터가 계속 들어가고 있으니.
          if(_html5recorder.isrecording == true){ 
              _html5recorder.recordedBlobs.push(event.data);
              if(rand == 50){                    
                  console.log('push'+ _html5recorder.recordedBlobs.length);
                  //console.log("push");
              }        
          }
        }  
    }
    /*
    * 답변을 저장한다.
    */
   public answerSave() {
        console.log("answerSave()");
        var $deferred = $.Deferred();

        if( _html5recorder.recordedBlobs == '' ){
            $deferred.resolve();
            return $deferred.promise();        
        }
        console.log(_html5recorder.mediaRecorder,'media recorder is deleted?');          
        console.log('업로드전blob 길이'+ _html5recorder.recordedBlobs.length);
        //TODO 업로드 게이지 설정 
        var fd = new FormData();
        var blob = new Blob(this.recordedBlobs, { type: 'video/mp4' });      
        var time = Math.floor(new Date().getTime() / 1000);
        fd.append('userseq',_html5recorder.request.userseq as string);
        fd.append('answersetseq',_html5recorder.json_data.answersetseq as string);
        fd.append('currentposition',(_html5recorder.currentpositionQuestion+1).toString());
        fd.append('sound100array', _html5recorder.sound100array as any );
        fd.append('fname', 'record_'+_html5recorder.request.userseq+'_'+time+'.mp4');      
        fd.append('data', blob);

        _html5recorder.calcing();

        $.ajax({
            type: 'POST',
            url: '/class/html5recorder_save.html',
            data: fd,
            processData: false,
            contentType: false
        }).done(function(data) {          
            _html5recorder.calcingend();
            console.log(x,'save end');
            var x = data;
            if(x=='' || x==undefined || x=='null' || x==null)
                return;
            var xp = jQuery.parseJSON(x);
            console.log(xp,'save end parse');
            if(xp['result'] == '0'){
                alert(xp['resulttext']);
                $deferred.reject();                
            }
            else if(xp['result'] == '1'){
                console.log(xp['movieurl'],'movieurl');                                
                _html5recorder.recordedBlobs = null;
                //console.log('업로드후blob 길이'+ _html5recorder.recordedBlobs.length);
                _html5recorder.sound100array = new Array();
                $deferred.resolve();
            }
            
        });
          
          
        return $deferred.promise();

    }
    calcing(){
        if( $('#calcingdiv').length <=0 ){
            var htmltext = '<div id="calcingdiv" style="display:none"><img src="/images/calcing.gif" style="width:200px;" alt="" id="calcingimg"/></div>';
            $(htmltext).appendTo('body');
        }
        var diffTop=Math.max(document.documentElement.scrollTop,document.body.scrollTop);
        var yplus=0;
        if(window.outerHeight != undefined){
            yplus=Math.min(window.outerHeight/2,jQuery(window).height()/2);//익스플로러 창크기 /2
        }
        else{
            yplus = jQuery(window).height()/2;
        }
        var x=jQuery(document).width()/2 - jQuery('#calcingimg').outerWidth()/2;  //x축 가운데
        var y=diffTop+yplus-jQuery('#calcingimg').outerHeight()/2; //y축 가운데;
        
        $('#calcingdiv').css('display','').css('position','absolute').css('top',y).css('left',x).css('z-index','100').css('display',''); 
    }
    calcingend(){
        $('#calcingdiv').css('display','none');  
    }
    /*
    * wait_time을 화면에 그린다.
    */
    public wait_time_draw(wait_time : number) {        
        let $wait_time_text = $('#waittimetext');
        console.log("남은시간:"+wait_time);
        if($wait_time_text.length <=0) {
            let waittimehtml = '<span id="waittimetext" style="color:white;position:absolute;top:0px;left:0px;"></span>';
            $(waittimehtml).appendTo('#main');
            $wait_time_text = $('#waittimetext');
        }
        $wait_time_text.text(this.timetotext(wait_time));
    }
    public timetotext(wait_time : number) {
        let wait_time_string = wait_time.toString();
        if(wait_time < 60) {
            if(wait_time_string.length == 1) {
                return '00:0'+wait_time_string;
            } else if(wait_time_string.length == 2) {
                return '00:'+wait_time_string;
            }
        } else if( wait_time >=60) {
           let minute = Math.floor(wait_time / 60);
           let second = wait_time % 60;
           let minutestring = minute.toString();
           let secondstring = second.toString();
           let minutetext = '';
           let secondtext = '';
           minutetext = minutestring;
           if(minutestring.length == 1) {
               minutetext = '0'+minutestring;
           } 
           secondtext = secondstring;
           if(secondstring.length == 1) {
               secondtext = '0'+secondstring;
           } 
           return minutetext+':'+secondtext;
        }    
    }
    /*
    * 현재 위치의 toronqtime만큼 준비시간을 줌. promise
    */    
    public toronqtime() {
        var $deferred = $.Deferred();
        let toronqtime = this.json_data['qulist'][ this.currentpositionQuestion ]['toronqtime'];
        if(!toronqtime) {
            this.log('toronqtime이 없습니다.position:'+this.currentpositionQuestion,'red');
            alert('대기시간이 없습니다. 오류입니다.');            
            $deferred.resolve();
            return $deferred.promise();            
        }

        //숫자표시        
        this.toronqtimeDraw(toronqtime);
        //1초씩 감소.0초가되면 resolve        
        let toronqtime_decrese_timer = setInterval( ()=>{
            toronqtime--;
            this.toronqtimeDraw(toronqtime);
            if(toronqtime <=0 ) {
                clearInterval(toronqtime_decrese_timer);
                toronqtime_decrese_timer = null;
                return $deferred.resolve();
            }
        }, 1000);
        return $deferred.promise();
    }
    /*
    * 준비시간을 화면에 그려준다.
    */
    public toronqtimeDraw(toronqtime) {
        let $toronqtimetext = $('#toronqtimetext');
        if($toronqtimetext.length <= 0) {            
            //flexdiv에 추가해줌. 가운데에서 y축을 조금 위로 올린다.
            let toronqtimehtml = '<span style="font-size:200px;padding-bottom:100px;color:white;text-shadow:-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;" id="toronqtimetext">'+toronqtime+'</span>';
            $(toronqtimehtml).appendTo('#flexdiv');
            $toronqtimetext = $('#toronqtimetext');            
        } else {
            if(toronqtime <= 0) {
                $toronqtimetext.css('display','none');
            } else {
                $toronqtimetext.css('display','');
            }
            $toronqtimetext.text(toronqtime);
        }        
    }
    
    
    public completeend() {
        var $deferred = $.Deferred();
        $.ajax({
          type: "POST",
          url: "/interview/getSetInfo2019.ajax.html",
          data: "",
          success: function(path) {
              console.log("completeend  ajax end");
              return $deferred.resolve();
          }
        });
        return $deferred.promise();

    }

    public add_mousehandler() {
        _html5recorder.mousehandler_added = true;
        $(document).on('mousemove', function(event) {
          _html5recorder.mousex = event.pageX;
          _html5recorder.mousey = event.pageY;     
          //console.log(_html5recorder.mousex+':'+_html5recorder.mousey)     ;
        });      

        $(document).on('mousedown', function(event) {    
            console.log('mousedown');
            _html5recorder.dragging = true   ;  
            _html5recorder.mouse_starty = event.clientY;  
            if(_html5recorder.textansweralert_dragabled == true) {
                //textanswer가 추가되었을때, 마우스를 클릭한 곳을 특정함. 
                if(_html5recorder.mouseclickable == true) {
                    //위쪽 선 클릭
                    console.log("선클릭");
                    _html5recorder.textansweralertissizeup = true;
                    _html5recorder.mouse_startheight =  $('#textansweralert2').outerHeight() ;
                    _html5recorder.mouse_starttop = parseInt( $('#textansweralert2').css('top') );
                    return;
                }
                let textansweralert = $('#textansweralert2');
                let textansweralertwidth = textansweralert.outerWidth();
                let textansweralertheight = textansweralert.outerHeight();
                if(textansweralert.length > 0) {
                    let offset = textansweralert.offset();
                    if( event.clientX > offset.left && event.clientX < (offset.left + textansweralertwidth) &&
                        event.clientY > offset.top && event.clientY < (offset.top + textansweralertheight) )
                     {
                         console.log("네모 안쪽 클릭");
                         _html5recorder.textansweralertismoving = true;     
                     }
                }

                _html5recorder.mouse_starttop = parseInt( $('#textansweralert2').css('top') );  
            }
            
            
        }); 
        $(document).on('mousemove', function(event) {             
            if(_html5recorder.dragging == true) {
                if(_html5recorder.textansweralertismoving == true) {     
                  //위아래로 움직임             
                  event.preventDefault();
                  let moving = _html5recorder.mouse_starty - event.clientY;                  
                  $('#textansweralert2').css('top', _html5recorder.mouse_starttop - (moving) );
                }
                if(_html5recorder.textansweralertissizeup == true ) {      
                  //위의 사이즈를 늘림. top이 올라가고, height도 늘어남.            
                  event.preventDefault();
                  let moving = _html5recorder.mouse_starty - event.clientY;
                  console.log('heightresize moving:'+moving);
                  console.log(_html5recorder.mouse_startheight + (moving) , 'height');
                  console.log(_html5recorder.mouse_starttop - (moving)  , 'top');
                  $('#textansweralert2').css('top', _html5recorder.mouse_starttop - (moving) );
                  $('#textansweralert2').css('height',_html5recorder.mouse_startheight + (moving) );  
                  $('#textansweralert2_text').css('height',_html5recorder.mouse_startheight + (moving) );  

                }
            }
        }); 
        $(document).on('mouseup', function(event) {
            console.log('mouseup');
            if(_html5recorder.dragging == true) {
                event.preventDefault();
                _html5recorder.dragging = false;
                _html5recorder.mouse_starty = 0;
                _html5recorder.mouse_starttop = 0;
            }
            _html5recorder.textansweralertismoving = false;
            _html5recorder.textansweralertissizeup = false;
        }); 
    
    }
    /*
    * 페이즈 추가.
    */
    public phaseAdd(phase : string) {
        if(phase == undefined || phase == '' || phase == null) {
            this.log('phase '+phase + '는 추가할 수 없습니다','color:red');
            return;
        }
        this.phaseChain.push(phase);
    }
    /*
    * 비디오 위치조정. 위치조정후 보이게 해준다. #my_video_player
    * x,y,width,height : String  {n}px 또는 center
    */  
    public myvideoPlace(x ='', y='', width='', height='') {
        let target = $('#'+this.myvideoid);
        if(x == 'center') {
            if(width == '') {
                this.log("가운데정렬시에 width가 있어야 합니다. 없어서 정렬하지 않았습니다.(myvideoPlace)",'red');
            }
            else {
                let windowwidth = $(window).width();        
                let documentwidth = $(document).width();        
                let windowouterwidth= $(window).outerWidth();        
                let documentouterwidth = $(document).outerWidth();        
                let maxwidth = Math.max(windowwidth,documentwidth,windowouterwidth,documentouterwidth);  
                let center = maxwidth/2 - (parseInt(width)/2);
                target.css('position','absolute');
                target.css('left',center);
            }
        }
        else if(x != '') {            
            target.css('position','absolute');
            target.css('left',x);
        }        
        if(y != '') {            
            target.css('position','absolute');
            target.css('top',y);
        }
        if(width != '') {
          target.css('position','absolute');
          target.css('width',width);
          $('#my_video_player').css('width',width);   //parent와 video태그의 크기를 둘다 줄인다.
        }
        if(height != '') {
          target.css('position','absolute');
          target.css('height',height);
          $('#my_video_player').css('height',height); //parent와 video태그의 크기를 둘다 줄인다.
        }        
    }  
    /*
    * 비디오parent의 X위치를 가운데로한다. absolute로 조정
    */
    public myvideoPlaceCenterX() {
        let windowwidth = $(window).width();        
        let documentwidth = $(document).width();        
        let windowouterwidth= $(window).outerWidth();        
        let documentouterwidth = $(document).outerWidth();        
        let maxwidth = Math.max(windowwidth,documentwidth,windowouterwidth,documentouterwidth);

        console.log(windowwidth,'windowwidth');
        console.log(documentwidth,'documentwidth');
        console.log(windowouterwidth,'windowouterwidth');
        console.log(documentouterwidth,'documentouterwidth');
        console.log(maxwidth , 'maxwidth');
        let x = maxwidth/2 - this.width/2 ;        
        $('#'+this.myvideoid).css('width',this.width).css('position','absolute').css('left',x+'px');
    }     
    /*
    * 사운드바를 생성 . 기본위치는 비디오 아래이다.
    */
    public soundBarMake() {
        if(this.myvideo == '') {
            this.log('video가 생성되지 않아서 사운드바를 생성하지 않음(soundBarMake())','red');
            return;
        }      
        let top = $('#'+this.myvideoid).position().top;
        let left = $('#'+this.myvideoid).position().left;
        let width = this.width;
        let height = this.height;
        if($('#voidesizediv').length > 0) {
            $('#voidesizediv').remove();
        }
        let htmltext = '<div id="voicesizediv" style="z-index:2;;font-size:18px;">\r\n\
                            <meter high="0.25" max="1" value="0" style=";width:100%"></meter>\r\n\
                        </div>';                        
        $(htmltext).appendTo('#main');
        let voicesizediv = $('#voicesizediv');
        voicesizediv.css('position','absolute');
        voicesizediv.css('top',top+height);
        voicesizediv.css('left',left);
        voicesizediv.css('width',width);
    }
   
    /*
    * 사운드바 위치조정 #soundbar
    */
    public soundBarPlace(x,y,width,height) {
        $('#soundbar').css('position','absolute').css('top',x).css('left',y).css('width',width).css('height',height).css('display','');
    }
    /*
    * 페이즈가 셋팅되어있다면, 초기화한다.  TODO
    */
    public phaseInit() {
        this.phaseChain = new Array();
        this.phaseChainSub = new Array();
    }
    /**
    * 한번만 동작. getsetinfo
    * $_type이 SET일때 no 와 lang을 받음
    * $_type 이 COLLEGE일때 ids,no,lang,type_select를 받음
    * $_type 이 KIUP이나 IR등등 그이외의 모든것일때 ids,no,lang,type,favoritejikjong,favoritekiup,coupon,gender,mywidthmyheight,swfversionanswersetseq,anlist,competitionseq,categorytarget,irseq,nocamera,ncsseq,intro,outro,cheating을 받음
    */
    public getsetinfoPromise(requestdata : RequestData) {        
        if( _html5recorder.getRequestExecuted === false) {
            _html5recorder.getRequest(requestdata);   
        } 

        var $deferred = $.Deferred();      
        if(this.b_getsetinfoed == true){            
            return $deferred.resolve();    
        }
        else{          
            //type이 SET일때 no, lang, isset=set
            //type이 COLLEGE일떄 ids, no, lang , college=1, type_select =  x              
            this.questiondata = new Object();            
            let questiondatalength = 0;
           
            let i=0;
            for(i=0; i < this.qu_list.length; i++) {
                this.questiondata[questiondatalength] =  new Object();
                this.questiondata[questiondatalength]['seq'] = this.qu_list[i];
                this.questiondata[questiondatalength]['url'] = '';
                this.questiondata[questiondatalength]['title'] = '';
                this.questiondata[questiondatalength]['width'] = '';
                this.questiondata[questiondatalength]['height'] = '';
                this.questiondata[questiondatalength]['duration'] = '';
                this.questiondata[questiondatalength]['lang'] = '';
                this.questiondata[questiondatalength]['hashtag'] = '';
                this.questiondata[questiondatalength]['uhyung'] = '';
                this.questiondata[questiondatalength]['file'] = '';
                this.questiondata[questiondatalength]['id'] = '';
                this.questiondata[questiondatalength]['striptitle'] = '';
                this.questiondata[questiondatalength]['categorytext'] = '';
                this.questiondata[questiondatalength]['toronqtime'] = '';
                this.questiondata[questiondatalength]['minrectime'] = '';
                this.questiondata[questiondatalength]['wait_time'] = '';
                this.questiondata[questiondatalength]['startmp4'] = '';
                this.questiondata[questiondatalength]['doinganswer'] = '';
                this.questiondata[questiondatalength]['endmovie'] = '';                
                questiondatalength++;

                if(this.qu_list[i] == undefined ) {
                    alert('오류. 질문인덱스 '+i+' 번째 질문번호가 없습니다.');
                    $deferred.reject();
                    return;
                }
            }
            this.questiondata["listcount"] = questiondatalength; 
            
            $.ajax({
                type: "POST",
                url: "/interview/getSetInfo2019.ajax.html",                
                data: {questiondata:this.questiondata},
                success: function(path) {
                    console.log(path,' getsetinfo2019 no parse');
                    if(path == ''){
                        alert('오류가 발생했습니다');
                        return;
                    }
                    _html5recorder.log('getsetinfo end','green');
                    //_html5recorder.log(unescape(path.replace(/\\/g, "%")),'green');  무슨코드지?
                    _html5recorder.json_data = jQuery.parseJSON(path);    
                    console.log(_html5recorder.json_data, ' getsetinfo2019 parse');                    
                    _html5recorder.getSetInfoSuccess(_html5recorder.json_data);
                    _html5recorder.b_getsetinfoed = true;
                    return $deferred.resolve();
                }
            });
            

        } // if(b_getsetinfoed ==true ) end
        console.log("promise return");
        return $deferred.promise();
    }
    /*
    * 지정되어있는 페이즈를 시작한다. 
    */
   public phaseStart() {
        if(this.b_getsetinfoed == false) {
            this.log("getsetinfo가 끝나기전에 phase가 start됨",'red');
        }
        console.log("phaseStart");
        this.nextPhase();        
   }
   /*
   * 서브페이즈중 질문재생페이즈. 앞선 화면을 정리하고 시작함.
   */
   public sub_questionPlaySubPhase() {     
       console.log('sub_questionPlaySubPhase');
       $('#'+this.playervideoid).html('');       
       var videoid = this.createVideo('_html5recorder.sub_questionPlaySubPhaseShift()'); 
       let url = this.json_data["qulist"][this.currentpositionQuestion]["url"];       
       console.log(this.currentpositionQuestion + '번째 질문 셋팅. url:'+url);       
       if(url == '' || url == undefined) {
           alert('오류:질문의 url이 없습니다.');
           this.log("오류:질문의 url이 없습니다.",'red');
           this.sub_questionPlaySubPhaseShift();
       }
       
       let videoplayer : HTMLMediaElement = $('#'+videoid)[0] as HTMLMediaElement;
      
       $('#'+videoid+'_source').attr('src',url);

       //1초간 networkstate와 readystate가 변하는지 확인. 변했다면 정상. 변하지않았다면 오류로 판단한다. 질문은 1초보다 짧은것이 있음. 다음페이즈로 넘어갔다면, 오류로 보지 않는다.      
       let subphaseAtTimerStart = this.currentsubphase;
        setTimeout( ()=>{
          console.log( videoplayer.networkState , 'networkstate'); //https://www.w3schools.com/tags/av_prop_networkstate.asp
          console.log( videoplayer.readyState , 'readyState');     //https://www.w3schools.com/tags/av_prop_readystate.asp          
          //비디오 상태가 변하지 않고, 서브페이즈도 그대로라면 오류가 나서 멈춘것으로 본다.
          console.log(subphaseAtTimerStart ,'subphaseAtTimerStart ');
          console.log(this.currentsubphase , 'this.currentsubphase');
          if( subphaseAtTimerStart == this.currentsubphase && videoplayer.networkState == 3 && videoplayer.readyState == 0) {
              if( !confirm("질문이 정상적으로 진행되고 있습니까?") ) {
                alert('오류가 발생하여 다음으로 넘어갑니다.');
                this.sub_questionPlaySubPhaseShift();
              }            
          }
        }, 1000 );
   }
   /*
   * 대기중영상이 있을때 재생해준다. 무한반복. 다음질문으로 넘어가면서 초기화할때 없애준다. 동영상을 로드 한 뒤, 대기하지 않고 다음으로 진행한다.
   */
   public sub_movingmp4SubPhase() {
       let movingmp4 = this.json_data["qulist"][ this.currentpositionQuestion ]["doinganswer"];
       if(movingmp4 == '' || movingmp4 == undefined) {
          this.sub_questionPlaySubPhaseShift();
          return;
       }
       console.log('currnetposition:'+this.currentpositionQuestion);
       console.log(movingmp4,'movingmp4');
       if(this.playervideoid == '') {
           this.log("플레이어 아이디가 없어서 대기중영상을 재생하지 않음","color:red");
           this.sub_questionPlaySubPhaseShift();
           return;
       }
       let videoid = this.createVideo();              //ex)video_player       
       console.log('videoid:'+videoid);
       $('#'+videoid).attr('loop','loop');    //루프일경우 onended가 필요하지 않음. 대기중인 영상은 무한반복한다.
       $('#'+videoid+'_source').attr('src',movingmp4);
       
       //1초뒤에 검사하여, 재생중이 아니라면 오류로 처리한다.
       let videoplayer : any = document.getElementById(videoid);
        setTimeout( ()=>{
          console.log( videoplayer.networkState , 'networkstate'); //https://www.w3schools.com/tags/av_prop_networkstate.asp
          console.log( videoplayer.readyState , 'readyState');     //https://www.w3schools.com/tags/av_prop_readystate.asp
          if(videoplayer.networkState == 3 && videoplayer.readyState == 0) {
              //movingmp4가 재생되지 않아도 문제를 계속 진행하도록 중지하지 않는다.
              this.log("movingmp4가 재생되지 않고있음. position:"+ this.currentpositionQuestion,'red');
          }
        }, 1000 );
        
       this.sub_questionPlaySubPhaseShift();
   }

   /*
   * 작성해둔 텍스트 답변이 있을때, 힌트로 화면에 보여주는 서브페이즈. 로드되면 화면에 보여준다. Ajax이지만 대기하지 않고 다음으로 진행해준다.면접실 번호가 있을때만, 보여준다.
   */
   public sub_textanswerviewSubPhase() {
        console.log('sub_textanswerviewSubPhase');
        if(this.request.irseq == '') {
            this.log(' 면접실 번호 없음. 답변텍스트는 출력하지 않는다.','yellow');
            _html5recorder.sub_questionPlaySubPhaseShift(); //ajax 결과를 대기하지 않고 진행함. 
            return;
        }    

        var url = '/interview/loadexercisetext.ajax.php';
        var dataarray = {json_data:this.json_data, num:this.currentpositionQuestion, irseq:this.request.irseq};
        $.ajax({
            type:'POST',
            url:url,
            data:dataarray,
            dataType:'html',
            cache:false,
            success:function(x,textStatus){
                    if(x=='' || x==undefined || x=='null' || x==null)
                      return;
                    var xp = jQuery.parseJSON(x);
                    console.log(xp,' loadexercisetext result parse');
                    if(xp['result'] == '0'){
                        alert(xp['resulttext']);
                    }
                    else if(xp['result'] == '1'){                        
                        if(xp['content'] != ''){
                            console.log(xp['content'],'content');
                            _html5recorder.textansweralert_add();
                            var nl2brtext = xp['content'].replace(/\n/g,'<br/>');
                            _html5recorder.textansweralert_content(nl2brtext);
                            console.log('textanswersee');
                            _html5recorder.textansweralert_see();
                            
                        }
                        else{                
                            _html5recorder.textansweralert_hide();                                                             
                        }
                    }
            },
            error:function(xhr,textStatus,errorThrown){
                    this.log('An error occurred!' + (errorThrown ? errorThrown : xhr.status),url);
            }
          }); 

          _html5recorder.sub_questionPlaySubPhaseShift(); //ajax 결과를 대기하지 않고 진행함. 
   }
   //답변텍스트를 보여주는 창을 DOM에 넣어준다.
   public textansweralert_add() {
       if($('#textansweralert2').length > 0) {
           return;
       }       
       let htmltext = '';
       htmltext = '<div id="textansweralert2" style=";background:transparent;;;;;font-size:36px;;;"  >\
                    <div id="textansweralert2_text" style=";opacity:0.9;text-align:center;font-size:36px;color:white;;line-height:1.5;height:300px;overflow-y:scroll;text-shadow:-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;"></div>\
                    <div style="width:100%;text-align:center;cursor:pointer;color:white"  id="textansweralert2_trianglebtn">△</div>\
                </div>';
       $(htmltext).appendTo('body');

       _html5recorder.textansweralert_dragable();
   }
   
   public textansweralert_dragable() {
       _html5recorder.textansweralert_dragabled = true;
       $('#textansweralert2').on('mouseover', function() {
          $('#textansweralert2').css('border','1px solid #abcdef');
       });
       $('#textansweralert2').on('mouseout', function() {
          $('#textansweralert2').css('border','0px');
       });
       //이것의 y축과 마우스y축이 같아지면 누를수 있게 변한다.
       $(document).off('mousemove', _html5recorder.textansweralert_dragable_mouseevent);
       $(document).on('mousemove', _html5recorder.textansweralert_dragable_mouseevent);       
   }
   public textansweralert_dragable_mouseevent(event) {
      if(_html5recorder.textansweralert_dragabled == true) {
          let top = $('#textansweralert2').offset().top;
          if( _html5recorder.mousey <= top+5 && _html5recorder.mousey >= top-5 ) {
              $('body').css('cursor','n-resize');
              _html5recorder.mouseclickable = true;
          } else {
              $('body').css('cursor','default'); 
              _html5recorder.mouseclickable = false;
          }
      }
   }
   public textansweralert_content( nl2brtext : string ) {
       $('#textansweralert2_text').html(nl2brtext);
   }
   public textansweralert_hide() {
       $('#textansweralert2').css('display','none');
       $('#textansweralert2_textarea').val('');                        
   }
   public textansweralert_see() {
        var diffTop=Math.max(document.documentElement.scrollTop,document.body.scrollTop);
        var yplus=jQuery(window).height()/2;//익스플로러 창크기 /2
        var x=jQuery(document).width()/2 - jQuery('#textansweralert').outerWidth()/2;  //x축 가운데
        var y=diffTop+yplus-jQuery('#textansweralert').outerHeight()/2; //y축 가운데;
        var widthh = jQuery(document).width() /2;
        var heightt = jQuery(document).height()/2;    
        
        $('#textansweralert2_text').css('font-size','36px');
        $('#textansweralert2').css('opacity','0').css('position','absolute').css('display','').css('top','20000').css('left','0');
        
        this.textansweralert_centerview();    
        this.textanswer2_heightcheck_timerset();
        
        $('#textansweralert2_trianglebtn').text('▲').off().on('click',this.textansweralert2_tosmall);
   }
   public textanswer2_heightcheck_timerset() {
       //텍스트높이가 일정이상이면 폰트를 줄인다.
      if(this.textanswer2_heightcheck_timer != null){
          clearInterval(this.textanswer2_heightcheck_timer);
          this.textanswer2_heightcheck_timer = null;
      }
      this.textanswer2_heightcheck_timer = setInterval( _html5recorder.textanswer2_heightcheck,100);
   }
   /*
   * 높이 기본값은 300px인데, 그보다 내용이 커서 스크롤이 생겼을경우, fontsize를 줄여준다.
   */
   public textanswer2_heightcheck() {
      var height = $('#textansweralert2').outerHeight();
      let scrollHeight = $('#textansweralert2_text').prop('scrollHeight');      
      var fontsizetext = '';
      let fontsize;
      
      if(scrollHeight > height){
          fontsizetext = $('#textansweralert2_text').css('font-size');
          fontsize = fontsizetext.replace('px','');
          fontsize = parseInt(fontsize);
          fontsize-=4;
          $('#textansweralert2_text').css('font-size',fontsize+'px');    
          console.log("textansweralert fontsize조절 현재사이즈:"+fontsize + 'height:'+height);
          _html5recorder.textansweralert_centerview();    
      }
      else{
          if(_html5recorder.textanswer2_heightcheck_timer != null){
              clearInterval(_html5recorder.textanswer2_heightcheck_timer);
              _html5recorder.textanswer2_heightcheck_timer = null;
              $('#textansweralert2').css('opacity','1');
              $('#textansweralert2_text').css('overflow','hidden');
          }
      }
   }
   public textansweralert2_tosmall() {
        $('#textansweralert2_text').css('display','none');
        $('#textansweralert2_trianglebtn').text('▼').off().on('click',_html5recorder.textansweralert2_tobig);   
   }
   public textansweralert2_tobig() {
        console.log('tobig');
        $('#textansweralert2_text').css('display','');
        $('#textansweralert2_trianglebtn').text('▲').off().on('click',_html5recorder.textansweralert2_tosmall);
   }
   public textansweralert_centerview() {
        var widthh = jQuery(document).width() /2;
        var heightt = jQuery(document).height()/2;    
        
        jQuery('#textansweralert2').css('width',widthh);
        var tempwidth = $('#textansweralert2').outerWidth();
        var tempheight = $('#textansweralert2').outerHeight();
        var centerx = widthh - tempwidth/2;
        var centery = heightt - tempheight/2;   
        centery += 100;
        
        jQuery('#textansweralert2').css('display','').css('position','absolute').css('top',centery).css('left',centerx).css('z-index','5000');
   }
   /*
   * 인트로 재생 메인 페이즈 TODO
   */
   public introPlayPhase() {
      console.log(_html5recorder.json_data, ' this jsondata');
      let introurl = this.json_data["intro"];
      if(introurl == undefined || introurl == '' || introurl == null) {
          this.nextPhase();  
          return;
      }
      if(!this.playervideoid) {      
          this.log("비디오재생 아이디가 설정되지 않아서 인트로를 재생하지 않습니다(introPlayPhase)","red");
          return;
      }
      this.log("인트로재생:"+introurl,'lightblue');
      var videoid = this.createVideo('_html5recorder.nextPhase()');              //ex)video_player
      let $videoplayer = $('#'+videoid);
      let videoplayer : any = document.getElementById(videoid);
      $('#'+videoid+'_source').attr('src',introurl);
      
      //1초뒤에 검사하여, 재생중이 아니라면 오류로 처리한다.
      setTimeout( ()=>{
        console.log( videoplayer.networkState , 'networkstate'); //https://www.w3schools.com/tags/av_prop_networkstate.asp
        console.log( videoplayer.readyState , 'readyState');     //https://www.w3schools.com/tags/av_prop_readystate.asp
        if(videoplayer.networkState == 3 && videoplayer.readyState == 0) {
            if( !confirm("인트로가 정상적으로 진행되고 있습니까?") ) {
              alert('오류가 발생하여 다음으로 넘어갑니다.');
              this.nextPhase();
            }            
        }
      }, 1000 );

      //myvideoplayer.onended = () => { console.log("end"); this.nextPhase(); };
      //myvideoplayer.addEventListener("ended", function(){alert('end')} )
      //myvideoplayer.onended = function() { console.log("end"); _html5recorder.nextPhase();  };
   }
   /*
   * 아우트로 재생 메인 페이즈 . 아우트로가 있을경우 재생한다. 
   */
   public outroPlayPhase() {
       console.log("outroPlayPhase");
       if(!this.json_data.outro) {
           this.nextPhase();
           return;
       }

       let videoid = this.createVideo("_html5recorder.nextPhase()");
       $('#'+videoid+'_source').attr('src',this.json_data.outro);
   }
   /*
   * 페이즈종료후 다음으로 넘어감
   */
   public nextPhase() {
      let currentphase = this.phaseChain.shift();
      //console.log(this.phaseChain,'phaseChain');
      this.currentphase = currentphase;
      console.log(currentphase,' currentphase');
      switch(currentphase) {
          case 'introPlay':
            this.introPlayPhase();
            break;
          case 'outroPlay':
            this.outroPlayPhase();
            break;
          case undefined:
          case 'null':
            console.log("메인페이즈 완전종료. 다음 페이지로 이동?")
            alert('모든과정이 종료되었습니다');

          break;
          default:
              let currentphase_split = currentphase.split(',');
              let currentphase_split_count = currentphase_split.length;
              let question_cnt = this.json_data["question_cnt"];
              let i = 0, j=0;
              this.phaseChainSub = new Array();
              if(question_cnt == undefined || question_cnt === 0) {
                  alert('오류. 질문개수가 설정되어있지 않습니다.');
                  this.log("nextPhase Error. 오류. 질문개수가 설정되어있지 않습니다.",'red');
                  this.sub_questionPlaySubPhaseShift();
                  return;
              }
              //json_data를 읽어서 질문개수만큼 push 한다. 꺼내어 쓴다. TODO
              if(currentphase_split[0] == "SELFVIEWINTERVIEW" ) {                  
                for(i=0; i< question_cnt; i++) {                    
                    for(j=1; j< currentphase_split_count; j++) {                        
                        this.phaseChainSub.push( currentphase_split[j] );
                    }                    
                }
                console.log(this.phaseChainSub,'phaseChain');
                this.sub_questionPlaySubPhaseShift();
              }
            break;  
      }
   }
   /*
   * 받은 인자를 내부의 request에 저장함
   */ 
   public getRequest(json : RequestData) {
        this.request = json;
        this.qu_list = new Array();
        this.qu_list_count = 0;
        this.qu_list_clip = '';
        this.qu_list_clip_alpha = '';
        for(var i in this.request){
            var value = this.request[i];                
            if( i.indexOf('qid') !== -1){                
                this.qu_list[this.qu_list_count] = value;
                this.qu_list_count++;
                this.qu_list_clip += value+'§';
                this.qu_list_clip_alpha += '＠'+value+'§';
            }
        }
        
        if( this.request.no == undefined){
            this.request.no = '';
        } 
        if( this.request.lang == undefined || this.request.lang == 'null'){
            this.request.lang = 'KR';            
        }
        
        if( this.request.type_select == undefined){
            this.request.type_select = '';
        }
        if( this.request.jikjongseq == undefined){
            this.request.jikjongseq = '';
        }
        if( this.request.favoritekiup == undefined){
            this.request.favoritekiup = '';
        }
        if( this.request.gender == undefined){
            this.request.gender = '';
        }
        if( this.request.answersetseq == undefined){
            this.request.answersetseq = '';
        }
        if( this.request.country == undefined){
            this.request.country = '';
        }
        if( this.request.question_cnt == undefined){
            this.request.question_cnt = 0;
        }
        if( this.request.competitionseq == undefined){
            this.request.competitionseq = '';
        }
        if( this.request.category == undefined){
            this.request.category = '';
        }
        if( this.request.target == undefined){
            this.request.target = ''; //인증타사
        }
        if( this.request.irseq == undefined){
            this.request.irseq = '';
        }
        if( this.request.ncsseq == undefined){
            this.request.ncsseq = '';
        }
        if( this.request.intro == undefined){
            this.request.intro = '';
        }
        if( this.request.outro == undefined){
            this.request.outro = '';
        }
        if( this.request.movingflv == undefined){
            this.request.movingflv = '';
        }
        if( this.request.userseq == undefined) {
            this.request.userseq = '';
        }
                
        if( this.request.answersetseq != ''){            
            //질문재도전 선택질문 재도전
            this.request.type = 'REINTERVIEW';
            if( this.request.lang == '' ){
                this.request.lang = this.request.country;
            }            
            if( this.request.question_cnt == 0 ){                
                alert('질문 개수를 찾지 못했습니다.');
                this.goback();
                return;
            }
            var qu_list_clip = '';
            var an_list_clip = '';
            var an_list = new Array();          
            let i : number;  
            for( i=1;  i <= this.request.question_cnt; i++){
                if(qu_list_clip != '')
                    qu_list_clip += '§';
                if(an_list_clip != '')
                    an_list_clip += '§';
                qu_list_clip += eval('this.request.qid_'+i);
                this.qu_list[i-1] = eval('this.request.qid_'+i);
                
                an_list_clip += eval('this.request.aid_'+i);
                an_list[i-1] = eval('this.request.aid_'+i);
            
                if( eval('this.request.qid_'+i) == '' ||  eval('this.request.qid_'+i) == undefined){
                    alert('질문이 부족해서 진행할 수 없습니다');
                    this.goback();
                    return;
                }               
            }
            this.an_list = an_list;
            this.an_list_clip = an_list_clip;
        }
        else{
            this.an_list = new Array();
        }
        this.getRequestExecuted = true;
    }
    /*
    * 사운드를 꺼준다. 에코방지
    */
    public soundOff() {        
        if(_html5recorder.myvideo) {            
            _html5recorder.myvideo.volume = 0;
            //_html5recorder.myvideo.muted = 0;
        }
    }
    public soundOn() {
        if(_html5recorder.myvideo) {
            _html5recorder.myvideo.volume = 1;   //0~1인거같다.
        }
    }
    public goback() {
        history.back();
    }
    /*
    * 콘솔로그함수, 텍스트, 색깔, 타이틀
    */
    public log(text : String,color : String ,data : String =''){
        if(data == '' || data == undefined){
            console.log('%c'+text,'color:'+color);
        }else{
            console.log('%c'+text,'color:'+color,data);
        }
    }
    /*
    * 초기화함수
    */
    public initializer() {
       /* window.stream;
        window.AudioContext;
        window.audioContext;
        window.soundMeter;
        sound100array;
        cameraErrorHandler
        cameraSuccessHandler
        
        currentsubphase
        currentphase
        textansweralert2

        recordedBlobs
        
        켜져있는 내 비디오를 끄고 this.myvideo 를 없앰
        */

        /*
        *redraw 할것
        toronqtimetext
        myvideoparent
        videoparent
        textansweralert2
        voicesizediv
        waittimetext
        $('#skipbutton').css('display','none');
        */
    }
    /*
    * 화면 초기화
    */
    public screenInitializer() {
         
    }
    public getUserMediaError() {
        if(location.protocol == 'http:'){
            alert('https로 접속해 주세요');
            location.protocol = 'https:';
            return;
        }
        else{ 
            _html5recorder.log('카메라 또는 오디오를 활성화하는데 실패했습니다.handleError(error)','red');
        }

        if(typeof _html5recorder.cameraErrorHandler == 'function'){        
            _html5recorder.cameraErrorHandler();
        }else{            
            _html5recorder.log('cameraerror()가 정의되지 않아서 실행하지 않음','red');
        }
    }
    /*
    * 내화면용 video를 생성함. this.myvideoid
    */
    public createMyVideo() {
        _html5recorder.log('createMyVideo()','green');
        if(this.myvideoid == ''){
            _html5recorder.log('createMyVideo오류. 비디오를 만들 대상의 아이디가 없습니다.','red');
            return;
        }    
        //이미 있는경우 생성하지 않음.
        if($('#my_video_player').length > 0) {
            return 'my_video_player';
        }
        
        $('#'+this.myvideoid).html('<video id="my_video_player" style="width:'+this.width+'px;height:'+this.height+'px;" onclick="_html5recorder.myvideo.play()" autoplay playsinline></video>');
        this.myvideo = $('#my_video_player')[0];
        return 'my_video_player';
    }
    /*
    * 재생용 video를 생성함. 있더라도 새로 만든다. onended가 바뀌기 때문.
    */
    public createVideo(onended:string = '') {
        
        $('#'+this.playervideoid).html('<video id="video_player" style="width:'+this.playvideowidth+'px;height:'+this.playvideoheight+'px;"  onended="'+onended+'"   autoplay playsinline>\
                                        <source src="" type="video/mp4" id="video_player_source">\
                                    </video>');  
        return 'video_player';    
    } 
    public getUserMediaSuccess(stream) {        
        window.stream = stream;
        if(_html5recorder.myvideo == '') {
            _html5recorder.createMyVideo();
        }
        _html5recorder.myvideo.srcObject = stream;
        console.log(_html5recorder.myvideo,'비디오 생성됨(getUserMediaSuccess)');        
        //에코방지
        _html5recorder.myvideo.volume = 0; 
        _html5recorder.myvideo.muted = 0; 
        
        
        //https://webrtc.github.io/samples/src/content/getusermedia/volume/
        if(window.audioContext == undefined){
            try {                   
                window.AudioContext = window.AudioContext || window.webkitAudioContext;
                window.audioContext = new AudioContext();
            } catch (e) {
                alert('Web Audio API not supported.');
            }
        }
        var soundMeter = window.soundMeter = new SoundMeter(window.audioContext);
        soundMeter.connectToSource(stream, function(e) {
            if (e) {
                alert(e);
            return;
            }
            setInterval(function() {         
                var sound=soundMeter.instant.toFixed(2);
                var sound100 = sound*100;
                /*if(_html5recorder.isrecording == 1 && _html5recorder.wait_timecount > 0 && _html5recorder.wait_timeTimerInstance != null){
                    //녹화중일때 기록한다.
                    _html5recorder.sound100array.push(sound100);
                    console.log(sound100);
                }*/
                
                $('#voicesizediv > meter').val(sound);          
            }, 100);
        });
        
        if(_html5recorder.cameraSuccessHandler) {
            setTimeout( () => {
              _html5recorder.cameraSuccessHandler();
            } , 1000  );            
        }
    }
    /*
    * 선택된 카메라가 바뀌었을때의 동작. 
    */
    public videoChange(){
        if(videoid == '')
            return;
            
        var videoidallstring : string = $('#settingModalVideoSelect').val() as string;
        var audioidallstring = $('#settingModalAudioSelect').val();
        if(videoidallstring == '' || videoidallstring == 'null' || videoidallstring == null)
            return;
        
        //장치/그룹/종류
        var videoidallstring_split = videoidallstring.split('/');        
        var videoid = videoidallstring_split[0];      
          
        if(audioidallstring != '' || videoidallstring == 'null' || videoidallstring == null) {
            var audioid = '';
            
        } else {
            var audioidallstring_split = audioidallstring.split('/'); 
            var audioid = audioidallstring_split[0];
        }
       
        if(videoid != '' && videoid != undefined){
            _html5recorder.selectedvideoid = videoid;
            
        }        
        if(audioid != '' && audioid != undefined){
            _html5recorder.selectedaudioid = audioid;            
        }
                
        console.log('videochange:'+videoidallstring + 'audio:'+audioid);
        
        if (window.stream) {
            window.stream.getTracks().forEach(function(track) {
              track.stop();
            });
        }
        
        if(audioid == ''){
            //비디오는 선택한것으로, 오디오는 자동     
            var constraints = {                       
                video: {
                  "width": _html5recorder.width,
                  "height": _html5recorder.height,
                  "frameRate": 30 ,
                  deviceId: {exact: videoid}
                }
                ,audio: {
                  "sampleSize": 16,
                  "channelCount": 2,
                  "echoCancellation": true                  
                }
              };
              console.log(constraints,'constraints');
              
              navigator.mediaDevices.getUserMedia(constraints).
                then(_html5recorder.getUserMediaSuccess).catch(_html5recorder.getUserMediaError);
                     
        }
        else if(audioid != '' && videoid != ''){
            //둘다 선택한 것으로

            var constraints2 = {                       
                video: {
                  "width": _html5recorder.width,
                  "height": _html5recorder.height,
                  "frameRate": 30 ,
                  deviceId: {exact: videoid}
                }
                ,audio: {
                  "sampleSize": 16,
                  "channelCount": 2,
                  "echoCancellation": true,
                  deviceId: {exact: audioid}
                }
              };

              console.log(constraints2);
              navigator.mediaDevices.getUserMedia(constraints2).
                then(_html5recorder.getUserMediaSuccess).catch(_html5recorder.getUserMediaError);
                          
        }          
    }

    /*
    * 선택된 오디오가 바뀌었을때 동작.
    */
    public audioChange() {
        var videoidallstring = $('#settingModalVideoSelect').val() as string;
        var audioidallstring = $('#settingModalAudioSelect').val() as string;
        if(audioidallstring == '' || audioidallstring == 'null' || audioidallstring == null)
            return;
        
        //장치/그룹/종류
        var videoidallstring_split = videoidallstring.split('/');        
        var videoid = videoidallstring_split[0];        
        var audioidallstring_split = audioidallstring.split('/'); 
        var audioid = audioidallstring_split[0];
        
                                
        if (window.stream) {
            window.stream.getTracks().forEach(function(track) {
              track.stop();
            });
        }
        console.log(videoid,' audiochange videoid');

        if(videoid == ''){
            //비디오는 자동, 오디오는 선택한것으로
            var constraints = {
                video: {
                  "width": _html5recorder.width,
                  "height": _html5recorder.height,
                  "frameRate": 30 
                },
                audio: {
                  "sampleSize": 16,
                  "channelCount": 2,
                  "echoCancellation": true,   
                  deviceId: {exact: audioid}
                }  
              };           
              navigator.mediaDevices.getUserMedia(constraints).then(_html5recorder.getUserMediaSuccess).catch(_html5recorder.getUserMediaError);
              console.log(constraints,'constraints');
        }
        else if(videoid != '' && audioid != ''){
            //둘다 선택한 것으로
            var constraints2 = {
                video: {
                  "width": _html5recorder.width,
                  "height": _html5recorder.height,
                  "frameRate": 30 ,      
                  deviceId : { exact: videoid }        
                },
                audio: {
                  "sampleSize": 16,
                  "channelCount": 2,
                  "echoCancellation": true,   
                  deviceId: {exact: audioid}
                }  
              };
              navigator.mediaDevices.getUserMedia(constraints2).then(_html5recorder.getUserMediaSuccess).catch(_html5recorder.getUserMediaError);              
              console.log(constraints2,'constraints2');
        }        
    }  
    /*
    * setting HTML을 없을시에 추가해준다.
    */
    public settingModalAdd() {
        if($('#settingModal').length > 0) {
            return;
        }
        var htmltext = '<div id="settingModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="settingModalLabel" aria-hidden="true">\
      <div class="modal-dialog">\
        <div class="modal-content">\
          <div class="modal-header">\
                <strong>카메라와 마이크 장치</strong>\
          </div>\
          <div class="modal-body">\
            카메라 장치: \
            <select id="settingModalVideoSelect" class="form-control input-sm" style="margin-top:5px" onchange="_html5recorder.videoChange()"></select>\
            <br/>\
            마이크 장치: \
            <select id="settingModalAudioSelect" class="form-control input-sm" style="margin-top:5px" onchange="_html5recorder.audioChange()"></select>            \
            <br/>\
            스피커 테스트<br/>\
            <div class="btn btn-default" onclick="_html5recorder.testsound()"><span style="color:#0067BC">▶</span> 테스트 소리 재생</div>            \
            <audio controls controlsList="nodownload" id="testsound" style="display:none">             \
              <source src="/sound/sound_test_kr.mp3" type="audio/mpeg">\
            Your browser does not support the audio element.\
            </audio>\
  \
          </div>\
          <div class="modal-footer">\
             <button type="button" class="btn btn-default" data-dismiss="modal" style="font-size:12px">닫기</button>            \
          </div>\
\
    </div>\
  </div>\
</div>';
        $(htmltext).appendTo('body');
    }
    /*
    * 셋팅 모달을 보여준다.
    */
    public settingModalShow() {
        if(!_html5recorder.myvideo) {
            _html5recorder.log("setting을 시작할 수 없습니다. video가 초기화되지 않았습니다.",'red');
            return;
        }
        _html5recorder.settingModalAdd();     

        navigator.mediaDevices.enumerateDevices().then(_html5recorder._loadAudioCameras);                        
    }
    testsound(){
        let player : HTMLMediaElement = $('#testsound').css('display','')[0] as HTMLMediaElement;
        player.play();  
	}
    /*
    * 카메라 종류를 로드해서 target에 select html로 넣어준다.
    */
    public _loadAudioCameras(deviceInfos : Array<any> ) : number {     
        console.log('loadAudioCameras');
        var videohtml = '',audiohtml = '';;        
        if(_html5recorder.selectedvideoid == ''){    
            videohtml = '<option value="">인식된 카메라</option>';
        }
        if(_html5recorder.selectedaudioid == ''){
            audiohtml = '<option value="">인식된 마이크</option>';
        }
        
        var isselected = '';
        var allstring = ''; //모든정보를 하나로 모음 slash로 구분
        console.log(deviceInfos,'deviceInfos');
        for (var i = 0; i !== deviceInfos.length; ++i) {
            var deviceInfo = deviceInfos[i];            
            var option = document.createElement('option');
            isselected = '';
            //allstring = deviceInfo.deviceId+'/'+deviceInfo.groupId+'/'+deviceInfo.kind;  //장치/그룹/종류
            allstring = deviceInfo.deviceId+'/'+deviceInfo.groupId+'/'+deviceInfo.kind+'/'+deviceInfo.label;  //장치/그룹/종류
            
            option.value = deviceInfo.deviceId;
            if (deviceInfo.kind === 'audioinput') {
                if(deviceInfo.deviceId == _html5recorder.selectedaudioid){
                    isselected = ' selected ';
                }
                audiohtml +='<option value="'+allstring+'" '+isselected+'>'+( deviceInfo.label || 'microphone '+(i+1) ) +'</option>';
            } else if (deviceInfo.kind === 'videoinput') {
                if(deviceInfo.deviceId == _html5recorder.selectedvideoid){
                    isselected = ' selected ';
                }
              videohtml +='<option value="'+allstring+'" '+isselected+'>'+( deviceInfo.label || 'camera '+(i+1) )  +'</option>';              
            } else if (deviceInfo.kind === 'audiooutput') {
                continue;
            } else {
              console.log('Found one other kind of source/device: ', deviceInfo);
            }
          }        
        $('#settingModalAudioSelect').html(audiohtml);
        $('#settingModalVideoSelect').html(videohtml);
        
        //만약 쿠키저장된게 있을때, 그 값을 선택해준다.
        /*var audioid = _html5recorder.getCookie('selfviewCameraAudio');
        var videoid = _html5recorder.getCookie('selfviewCameraVideo');
        if(audioid != '' && audioid != undefined) {            
            $('#settingModalAudioSelect').val(audioid).trigger('change');;
        }
        if(videoid != '' && videoid != undefined) {            
            $('#settingModalVideoSelect').val(videoid).trigger('change');
        }*/
        
        if(window.stream == undefined){
            alert('웹브라우저  ‘주소창’ 오른쪽에서 ‘카메라’ 아이콘을 선택해서 “계속 허용”을 선택하세요.');
            return;
        }
        $('#settingModal').modal('show'); 
         
        return 1;
    } 

    
  
  /**
    * getSetInfo ajax 끝난뒤 실행
    */
   getSetInfoSuccess(json_data){
      if(json_data != null){
          if(json_data.code == -1){
              alert(json_data.msg);
              this.goback();
              return;
          } 
      }
      else{
          alert('면접시작에 오류가 발생했습니다.');
          _html5recorder.log('getsetinfo에서 데이터가 생성되지 않음','red');
          return;
      }
    }

    /*
    * 개발을 위한 페이즈 스탑.메인과 서브페이즈를 전부 삭제한다.
    */
    phaseStopForTest() {
        this.phaseChainSub = new Array();
        this.phaseChain = new Array();
        this.nextPhase();
    }
}


//https://webrtc.github.io/samples/src/content/getusermedia/volume/
function SoundMeter(context) {
    this.context = context;
    this.instant = 0.0;
    this.slow = 0.0;
    this.clip = 0.0;
    this.script = context.createScriptProcessor(2048, 1, 1);
    var that = this;
    this.script.onaudioprocess = function(event) {
      var input = event.inputBuffer.getChannelData(0);
      var i;
      var sum = 0.0;
      var clipcount = 0;
      for (i = 0; i < input.length; ++i) {
        sum += input[i] * input[i];
        if (Math.abs(input[i]) > 0.99) {
          clipcount += 1;
        }
      }
      that.instant = Math.sqrt(sum / input.length);
      that.slow = 0.95 * that.slow + 0.05 * that.instant;
      that.clip = clipcount / input.length;
    };
}
  
SoundMeter.prototype.connectToSource = function(stream, callback) {
    try {
      this.mic = this.context.createMediaStreamSource(stream);
      this.mic.connect(this.script);
      // necessary to make sample run, but should not be.
      this.script.connect(this.context.destination);
      if (typeof callback !== 'undefined') {
        callback(null);
      }
    } catch (e) {
      console.error(e);
      if (typeof callback !== 'undefined') {
        callback(e);
      }
    }
};
SoundMeter.prototype.stop = function() {
    this.mic.disconnect();
    this.script.disconnect();
};


/*
vscode 에러회피방법 1과 interface를 정의하는 방법 2 https://ourcodeworld.com/articles/read/337/how-to-declare-a-new-property-on-the-window-object-with-typescript
if((<any>window).stream == undefined){
      alert('웹브라우저  ‘주소창’ 오른쪽에서 ‘카메라’ 아이콘을 선택해서 “계속 허용”을 선택하세요.');
      return;
  }
*/

interface Window {
  /*
  1방법 (<any>window).stream
  2방법 
  AudioContext : any;
  webkitAudioContext : any;
  audioContext : any;
  soundMeter : any;
  stream : any;*/

  [propName: string]: any; //문자열 인덱스 시그니쳐. 어떤 값을 가질수 있다. https://typescript-kr.github.io/pages/Interfaces.html
}
  
interface RequestData {
    no : number | ''; //선택한 번호. 기업선택일땐 기업번호, 직종선택일땐 직종번호     
    answersetseq : number | '';  //답변 재도전일때 답변셋번호
    question_cnt : number;  //질문개수
    competitionseq : number | ''; //경진대회 번호
    category : number | '';  //고교 전공선택시 카테고리 번호
    target : number | ''; //IROPENURL 로 로그인하지 않고 targetseq를 사용해서 면접을 시작. 로그인되어있을때는 로그아웃 해야함.
    irseq : number | ''; //면접실 번호
    ncsseq : number | ''; //ncsseq
    intro : string | '';  //인트로 영상 지정시
    outro : string | '';  //아우트로 영상 지정시.
    movingflv : string | ''; //대기중영상 지정시.
    type : string | '';   // REINTERVIEW
    homeworkseq: number | '';
    userseq : number | '';

    //metadata
    type_select : number | ''; // 고교에서,1~7, 인문계 사범계등등을 나타낸다
    jikjongseq : number | ''; // 선택한 직종번호
    favoritekiup : number | ''; //선택한 기업번호
    gender : string | ''; //성별

    //deprecated
    lang : string | '';  //질문마다 lang이 있어야함 answerset의 lang은 폐기함
    country : string | ''; //질문마다 lang이 있어야함 answerset의 lang은 폐기함. (country 는 lang에 대입하게되므로)
    cheating : number | '';  //1 text보기 2text안보기.
}

interface JSONDATA {
    qulist : Array<Object>;
    anlist : Array<Object>;
    question_cnt : number | '';
    answersetseq : number | '';
    outro : string | '';
    result : number;
    resulttext : string;
    resulttexteng : string | '';
    resulttextcn : string | '';
}

declare var MediaRecorder: any;
/*
getsetinfo REQUEST

REQUEST Array
(
    [questiondata] => Array
        (
            [0] => Array
                (
                    [seq] => 818
                    [title] => 우리 교육원을 지원하게 된 동기를 말해보세요.
                    [width] => 1280
                    [height] => 720
                    [duration] => 6
                    [lang] => KR
                    [hashtag] => #초중등-면접#남성
                    [uhyung] => 영상
                    [file] => /data/mp4/q/#초중등-면접#남성/우리교육원을지원하게된동기를말해보세요.mp4
                    [id] => OgEWc4XJprSt
                    [striptitle] => 우리교육원을지원하게된동기를말해보세요
                    [categorytext] => 자기소개.지원동기
                    [toronqtime] => 3
                    [minrectime] => 10
                    [wait_time] => 60
                    [startmp4] => https://www.selfview.com/collection/xS4X3lkfZ3YU/6f4378757365fdbee830d50183d72bcf05e3b44867a02719f039b58d97bdac70/6060985934391/startmp4
                    [doinganswer] => https://www.selfview.com/collection/xS4X3lkfZ3YU/6f4378757365fdbee830d50183d72bcf05e3b44867a02719f039b58d97bdac70/6060985934391/doinganswer
                    [endmovie] => https://www.selfview.com/collection/xS4X3lkfZ3YU/6f4378757365fdbee830d50183d72bcf05e3b44867a02719f039b58d97bdac70/6060985934391/endmovie
                    [url] => https://www.selfview.com/q/OgEWc4XJprSt/69fd89631367dabb3cafbdd58d3d17a0f74da96940619e1fc7e9c58d7ff03ba7/6060985934391
                )

            [1] => Array
                (
                    [seq] => 823
                    [title] => 우리 교육원이 지원자를 선발해야 하는 이유를 장래희망과 연관지어 말해보세요.
                    [width] => 1280
                    [height] => 720
                    [duration] => 9
                    [lang] => KR
                    [hashtag] => #초중등-면접#남성
                    [uhyung] => 영상
                    [file] => /data/mp4/q/#초중등-면접#남성/우리교육원이지원자를선발해야하는이유를장래희망과연관지어말해보세요.mp4
                    [id] => HcYeRoOoYBh6
                    [striptitle] => 우리교육원이지원자를선발해야하는이유를장래희망과연관지어말해보세요
                    [categorytext] => 자기소개.지원동기
                    [toronqtime] => 3
                    [minrectime] => 10
                    [wait_time] => 60
                    [startmp4] => https://www.selfview.com/collection/xS4X3lkfZ3YU/6f4378757365fdbee830d50183d72bcf05e3b44867a02719f039b58d97bdac70/6060985934391/startmp4
                    [doinganswer] => https://www.selfview.com/collection/xS4X3lkfZ3YU/6f4378757365fdbee830d50183d72bcf05e3b44867a02719f039b58d97bdac70/6060985934391/doinganswer
                    [endmovie] => https://www.selfview.com/collection/xS4X3lkfZ3YU/6f4378757365fdbee830d50183d72bcf05e3b44867a02719f039b58d97bdac70/6060985934391/endmovie
                    [url] => https://www.selfview.com/q/HcYeRoOoYBh6/8cd34915898448a4388646b5790cd728e74db46ac5984a1d25ec641354c00785/6060985934391
                )

            [listcount] => 2
            [result] => 1
            [resulttext] => success
            [resulten] => success
        )

    [collectionseq] => 15
    [mywidth] => 1920
    [myheight] => 1080
)
*/

/*
* getsetinfo RESULT

Array
(
    [qulist] => Array
        (
            [0] => Array
                (
                    [url] => https://www.selfview.com/q/OgEWc4XJprSt/69fd89631367dabb3cafbdd58d3d17a0f74da96940619e1fc7e9c58d7ff03ba7/6060985934391
                    [startmp4] => https://www.selfview.com/collection/xS4X3lkfZ3YU/6f4378757365fdbee830d50183d72bcf05e3b44867a02719f039b58d97bdac70/6060985934391/startmp4
                    [doinganswer] => https://www.selfview.com/collection/xS4X3lkfZ3YU/6f4378757365fdbee830d50183d72bcf05e3b44867a02719f039b58d97bdac70/6060985934391/doinganswer
                    [endmovie] => https://www.selfview.com/collection/xS4X3lkfZ3YU/6f4378757365fdbee830d50183d72bcf05e3b44867a02719f039b58d97bdac70/6060985934391/endmovie
                    [toronqtime] => 3
                    [wait_time] => 60
                    [minrectime] => 10
                    [quseq] => 818
                )

            [1] => Array
                (
                    [url] => https://www.selfview.com/q/HcYeRoOoYBh6/8cd34915898448a4388646b5790cd728e74db46ac5984a1d25ec641354c00785/6060985934391
                    [startmp4] => https://www.selfview.com/collection/xS4X3lkfZ3YU/6f4378757365fdbee830d50183d72bcf05e3b44867a02719f039b58d97bdac70/6060985934391/startmp4
                    [doinganswer] => https://www.selfview.com/collection/xS4X3lkfZ3YU/6f4378757365fdbee830d50183d72bcf05e3b44867a02719f039b58d97bdac70/6060985934391/doinganswer
                    [endmovie] => https://www.selfview.com/collection/xS4X3lkfZ3YU/6f4378757365fdbee830d50183d72bcf05e3b44867a02719f039b58d97bdac70/6060985934391/endmovie
                    [toronqtime] => 3
                    [wait_time] => 60
                    [minrectime] => 10
                    [quseq] => 823
                )

        )

    [anlist] => Array
        (
            [0] => Array
                (
                    [anseq] => 1203310
                )

            [1] => Array
                (
                    [anseq] => 1203311
                )

        )

    [question_cnt] => 2
    [answersetseq] => 260851
    [result] => 1
    [resulttext] => success
    [resulttexteng] =>
    [resulttextcn] =>
)
*/
